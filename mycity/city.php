<?php 
return array (
  $city=array(1 => 
  array (
    'province_name' => '北京',
  ),
  2 => 
  array (
    'province_name' => '天津',
  ),
  3 => 
  array (
    'province_name' => '河北',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '石家庄',
      ),
      2 => 
      array (
        'city_name' => '唐山',
      ),
      3 => 
      array (
        'city_name' => '秦皇岛',
      ),
      4 => 
      array (
        'city_name' => '邯郸',
      ),
      5 => 
      array (
        'city_name' => '邢台',
      ),
      6 => 
      array (
        'city_name' => '保定',
      ),
      7 => 
      array (
        'city_name' => '张家口',
      ),
      8 => 
      array (
        'city_name' => '承德',
      ),
      9 => 
      array (
        'city_name' => '沧州',
      ),
      10 => 
      array (
        'city_name' => '廊坊',
      ),
      11 => 
      array (
        'city_name' => '衡水',
      ),
    ),
  ),
  4 => 
  array (
    'province_name' => '山西',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '太原',
      ),
      2 => 
      array (
        'city_name' => '大同',
      ),
      3 => 
      array (
        'city_name' => '阳泉',
      ),
      4 => 
      array (
        'city_name' => '长治',
      ),
      5 => 
      array (
        'city_name' => '晋城',
      ),
      6 => 
      array (
        'city_name' => '朔州',
      ),
      7 => 
      array (
        'city_name' => '晋中',
      ),
      8 => 
      array (
        'city_name' => '运城',
      ),
      9 => 
      array (
        'city_name' => '忻州',
      ),
      10 => 
      array (
        'city_name' => '临汾',
      ),
      11 => 
      array (
        'city_name' => '吕梁',
      ),
    ),
  ),
  5 => 
  array (
    'province_name' => '内蒙古',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '呼和浩特',
      ),
      2 => 
      array (
        'city_name' => '包头',
      ),
      3 => 
      array (
        'city_name' => '乌海',
      ),
      4 => 
      array (
        'city_name' => '赤峰',
      ),
      5 => 
      array (
        'city_name' => '通辽',
      ),
      6 => 
      array (
        'city_name' => '鄂尔多斯',
      ),
      7 => 
      array (
        'city_name' => '呼伦贝尔',
      ),
      8 => 
      array (
        'city_name' => '巴彦淖尔',
      ),
      9 => 
      array (
        'city_name' => '乌兰察布',
      ),
      10 => 
      array (
        'city_name' => '兴安盟',
      ),
      11 => 
      array (
        'city_name' => '锡林郭勒盟',
      ),
      12 => 
      array (
        'city_name' => '阿拉善盟',
      ),
    ),
  ),
  6 => 
  array (
    'province_name' => '辽宁',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '沈阳',
      ),
      2 => 
      array (
        'city_name' => '大连',
      ),
      3 => 
      array (
        'city_name' => '鞍山',
      ),
      4 => 
      array (
        'city_name' => '抚顺',
      ),
      5 => 
      array (
        'city_name' => '本溪',
      ),
      6 => 
      array (
        'city_name' => '丹东',
      ),
      7 => 
      array (
        'city_name' => '锦州',
      ),
      8 => 
      array (
        'city_name' => '营口',
      ),
      9 => 
      array (
        'city_name' => '阜新',
      ),
      10 => 
      array (
        'city_name' => '辽阳',
      ),
      11 => 
      array (
        'city_name' => '盘锦',
      ),
      12 => 
      array (
        'city_name' => '铁岭',
      ),
      13 => 
      array (
        'city_name' => '朝阳',
      ),
      14 => 
      array (
        'city_name' => '葫芦岛',
      ),
    ),
  ),
  7 => 
  array (
    'province_name' => '吉林', 
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '长春',
      ),
      2 => 
      array (
        'city_name' => '吉林',
      ),
      3 => 
      array (
        'city_name' => '四平',
      ),
      4 => 
      array (
        'city_name' => '辽源',
      ),
      5 => 
      array (
        'city_name' => '通化',
      ),
      6 => 
      array (
        'city_name' => '白山',
      ),
      7 => 
      array (
        'city_name' => '松原',
      ),
      8 => 
      array (
        'city_name' => '白城',
      ),
      9 => 
      array (
        'city_name' => '延边',
      ),
    ),
  ),
  8 => 
  array (
    'province_name' => '黑龙江',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '哈尔滨',
      ),
      2 => 
      array (
        'city_name' => '齐齐哈尔',
      ),
      3 => 
      array (
        'city_name' => '鸡西',
      ),
      4 => 
      array (
        'city_name' => '鹤岗',
      ),
      5 => 
      array (
        'city_name' => '双鸭山',
      ),
      6 => 
      array (
        'city_name' => '大庆',
      ),
      7 => 
      array (
        'city_name' => '伊春',
      ),
      8 => 
      array (
        'city_name' => '佳木斯',
      ),
      9 => 
      array (
        'city_name' => '七台河',
      ),
      10 => 
      array (
        'city_name' => '牡丹江',
      ),
      11 => 
      array (
        'city_name' => '黑河',
      ),
      12 => 
      array (
        'city_name' => '绥化',
      ),
      13 => 
      array (
        'city_name' => '大兴安岭',
      ),
    ),
  ),
  9 => 
  array (
    'province_name' => '上海',
  ),
  10 => 
  array (
    'province_name' => '江苏',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '南京',
      ),
      2 => 
      array (
        'city_name' => '无锡',
      ),
      3 => 
      array (
        'city_name' => '徐州',
      ),
      4 => 
      array (
        'city_name' => '常州',
      ),
      5 => 
      array (
        'city_name' => '苏州',
      ),
      6 => 
      array (
        'city_name' => '南通',
      ),
      7 => 
      array (
        'city_name' => '连云港',
      ),
      8 => 
      array (
        'city_name' => '淮安',
      ),
      9 => 
      array (
        'city_name' => '盐城',
      ),
      10 => 
      array (
        'city_name' => '扬州',
      ),
      11 => 
      array (
        'city_name' => '镇江',
      ),
      12 => 
      array (
        'city_name' => '泰州',
      ),
      13 => 
      array (
        'city_name' => '宿迁',
      ),
    ),
  ),
  11 => 
  array (
    'province_name' => '浙江',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '杭州',
      ),
      2 => 
      array (
        'city_name' => '宁波',
      ),
      3 => 
      array (
        'city_name' => '温州',
      ),
      4 => 
      array (
        'city_name' => '嘉兴',
      ),
      5 => 
      array (
        'city_name' => '湖州',
      ),
      6 => 
      array (
        'city_name' => '绍兴',
      ),
      7 => 
      array (
        'city_name' => '金华',
      ),
      8 => 
      array (
        'city_name' => '衢州',
      ),
      9 => 
      array (
        'city_name' => '舟山',
      ),
      10 => 
      array (
        'city_name' => '台州',
      ),
      11 => 
      array (
        'city_name' => '丽水',
      ),
    ),
  ),
  12 => 
  array (
    'province_name' => '安徽',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '合肥',
      ),
      2 => 
      array (
        'city_name' => '芜湖',
      ),
      3 => 
      array (
        'city_name' => '蚌埠',
      ),
      4 => 
      array (
        'city_name' => '淮南',
      ),
      5 => 
      array (
        'city_name' => '马鞍山',
      ),
      6 => 
      array (
        'city_name' => '淮北',
      ),
      7 => 
      array (
        'city_name' => '铜陵',
      ),
      8 => 
      array (
        'city_name' => '安庆',
      ),
      9 => 
      array (
        'city_name' => '黄山',
      ),
      10 => 
      array (
        'city_name' => '滁州',
      ),
      11 => 
      array (
        'city_name' => '阜阳',
      ),
      12 => 
      array (
        'city_name' => '宿州',
      ),
      13 => 
      array (
        'city_name' => '巢湖',
      ),
      14 => 
      array (
        'city_name' => '六安',
      ),
      15 => 
      array (
        'city_name' => '亳州',
      ),
      16 => 
      array (
        'city_name' => '池州',
      ),
      17 => 
      array (
        'city_name' => '宣城',
      ),
    ),
  ),
  13 => 
  array (
    'province_name' => '福建',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '福州',
      ),
      2 => 
      array (
        'city_name' => '厦门',
      ),
      3 => 
      array (
        'city_name' => '莆田',
      ),
      4 => 
      array (
        'city_name' => '三明',
      ),
      5 => 
      array (
        'city_name' => '泉州',
      ),
      6 => 
      array (
        'city_name' => '漳州',
      ),
      7 => 
      array (
        'city_name' => '南平',
      ),
      8 => 
      array (
        'city_name' => '龙岩',
      ),
      9 => 
      array (
        'city_name' => '宁德',
      ),
    ),
  ),
  14 => 
  array (
    'province_name' => '江西',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '南昌',
      ),
      2 => 
      array (
        'city_name' => '景德镇',
      ),
      3 => 
      array (
        'city_name' => '萍乡',
      ),
      4 => 
      array (
        'city_name' => '九江',
      ),
      5 => 
      array (
        'city_name' => '新余',
      ),
      6 => 
      array (
        'city_name' => '鹰潭',
      ),
      7 => 
      array (
        'city_name' => '赣州',
      ),
      8 => 
      array (
        'city_name' => '吉安',
      ),
      9 => 
      array (
        'city_name' => '宜春',
      ),
      10 => 
      array (
        'city_name' => '抚州',
      ),
      11 => 
      array (
        'city_name' => '上饶',
      ),
    ),
  ),
  15 => 
  array (
    'province_name' => '山东',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '济南',
      ),
      2 => 
      array (
        'city_name' => '青岛',
      ),
      3 => 
      array (
        'city_name' => '淄博',
      ),
      4 => 
      array (
        'city_name' => '枣庄',
      ),
      5 => 
      array (
        'city_name' => '东营',
      ),
      6 => 
      array (
        'city_name' => '烟台',
      ),
      7 => 
      array (
        'city_name' => '潍坊',
      ),
      8 => 
      array (
        'city_name' => '济宁',
      ),
      9 => 
      array (
        'city_name' => '泰安',
      ),
      10 => 
      array (
        'city_name' => '威海',
      ),
      11 => 
      array (
        'city_name' => '日照',
      ),
      12 => 
      array (
        'city_name' => '莱芜',
      ),
      13 => 
      array (
        'city_name' => '临沂',
      ),
      14 => 
      array (
        'city_name' => '德州',
      ),
      15 => 
      array (
        'city_name' => '聊城',
      ),
      16 => 
      array (
        'city_name' => '滨州',
      ),
      17 => 
      array (
        'city_name' => '荷泽',
      ),
    ),
  ),
  16 => 
  array (
    'province_name' => '河南',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '郑州',
      ),
      2 => 
      array (
        'city_name' => '开封',
      ),
      3 => 
      array (
        'city_name' => '洛阳',
      ),
      4 => 
      array (
        'city_name' => '平顶山',
      ),
      5 => 
      array (
        'city_name' => '安阳',
      ),
      6 => 
      array (
        'city_name' => '鹤壁',
      ),
      7 => 
      array (
        'city_name' => '新乡',
      ),
      8 => 
      array (
        'city_name' => '焦作',
      ),
      9 => 
      array (
        'city_name' => '濮阳',
      ),
      10 => 
      array (
        'city_name' => '许昌',
      ),
      11 => 
      array (
        'city_name' => '漯河',
      ),
      12 => 
      array (
        'city_name' => '三门峡',
      ),
      13 => 
      array (
        'city_name' => '南阳',
      ),
      14 => 
      array (
        'city_name' => '商丘',
      ),
      15 => 
      array (
        'city_name' => '信阳',
      ),
      16 => 
      array (
        'city_name' => '周口',
      ),
      17 => 
      array (
        'city_name' => '驻马店',
      ),
    ),
  ),
  17 => 
  array (
    'province_name' => '湖北',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '武汉',
      ),
      2 => 
      array (
        'city_name' => '黄石',
      ),
      3 => 
      array (
        'city_name' => '十堰',
      ),
      4 => 
      array (
        'city_name' => '宜昌',
      ),
      5 => 
      array (
        'city_name' => '襄樊',
      ),
      6 => 
      array (
        'city_name' => '鄂州',
      ),
      7 => 
      array (
        'city_name' => '荆门',
      ),
      8 => 
      array (
        'city_name' => '孝感',
      ),
      9 => 
      array (
        'city_name' => '荆州',
      ),
      10 => 
      array (
        'city_name' => '黄冈',
      ),
      11 => 
      array (
        'city_name' => '咸宁',
      ),
      12 => 
      array (
        'city_name' => '随州',
      ),
      13 => 
      array (
        'city_name' => '恩施',
      ),
      14 => 
      array (
        'city_name' => '神农架',
      ),
    ),
  ),
  18 => 
  array (
    'province_name' => '湖南',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '长沙',
      ),
      2 => 
      array (
        'city_name' => '株洲',
      ),
      3 => 
      array (
        'city_name' => '湘潭',
      ),
      4 => 
      array (
        'city_name' => '衡阳',
      ),
      5 => 
      array (
        'city_name' => '邵阳',
      ),
      6 => 
      array (
        'city_name' => '岳阳',
      ),
      7 => 
      array (
        'city_name' => '常德',
      ),
      8 => 
      array (
        'city_name' => '张家界',
      ),
      9 => 
      array (
        'city_name' => '益阳',
      ),
      10 => 
      array (
        'city_name' => '郴州',
      ),
      11 => 
      array (
        'city_name' => '永州',
      ),
      12 => 
      array (
        'city_name' => '怀化',
      ),
      13 => 
      array (
        'city_name' => '娄底',
      ),
      14 => 
      array (
        'city_name' => '湘西',
      ),
    ),
  ),
  19 => 
  array (
    'province_name' => '广东',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '广州',
      ),
      2 => 
      array (
        'city_name' => '韶关',
      ),
      3 => 
      array (
        'city_name' => '深圳',
      ),
      4 => 
      array (
        'city_name' => '珠海',
      ),
      5 => 
      array (
        'city_name' => '汕头',
      ),
      6 => 
      array (
        'city_name' => '佛山',
      ),
      7 => 
      array (
        'city_name' => '江门',
      ),
      8 => 
      array (
        'city_name' => '湛江',
      ),
      9 => 
      array (
        'city_name' => '茂名',
      ),
      10 => 
      array (
        'city_name' => '肇庆',
      ),
      11 => 
      array (
        'city_name' => '惠州',
      ),
      12 => 
      array (
        'city_name' => '梅州',
      ),
      13 => 
      array (
        'city_name' => '汕尾',
      ),
      14 => 
      array (
        'city_name' => '河源',
      ),
      15 => 
      array (
        'city_name' => '阳江',
      ),
      16 => 
      array (
        'city_name' => '清远',
      ),
      17 => 
      array (
        'city_name' => '东莞',
      ),
      18 => 
      array (
        'city_name' => '中山',
      ),
      19 => 
      array (
        'city_name' => '潮州',
      ),
      20 => 
      array (
        'city_name' => '揭阳',
      ),
      21 => 
      array (
        'city_name' => '云浮',
      ),
    ),
  ),
  20 => 
  array (
    'province_name' => '广西',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '南宁',
      ),
      2 => 
      array (
        'city_name' => '柳州',
      ),
      3 => 
      array (
        'city_name' => '桂林',
      ),
      4 => 
      array (
        'city_name' => '梧州',
      ),
      5 => 
      array (
        'city_name' => '北海',
      ),
      6 => 
      array (
        'city_name' => '防城港',
      ),
      7 => 
      array (
        'city_name' => '钦州',
      ),
      8 => 
      array (
        'city_name' => '贵港',
      ),
      9 => 
      array (
        'city_name' => '玉林',
      ),
      10 => 
      array (
        'city_name' => '百色',
      ),
      11 => 
      array (
        'city_name' => '贺州',
      ),
      12 => 
      array (
        'city_name' => '河池',
      ),
      13 => 
      array (
        'city_name' => '来宾',
      ),
      14 => 
      array (
        'city_name' => '崇左',
      ),
    ),
  ),
  21 => 
  array (
    'province_name' => '海南', 
	
	
	
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '海口',
      ),
      2 => 
      array (
        'city_name' => '三亚',
      ),
      3 => 
      array (
        'city_name' => '三沙',
      ),
      4 => 
      array (
        'city_name' => '琼海',
      ),
      5 => 
      array (
        'city_name' => '五指山',
      ),
      6 => 
      array (
        'city_name' => '文昌',
      ),
	  7 => 
      array (
        'city_name' => '万宁',
      ),
	  8 => 
      array (
        'city_name' => '屯昌',
      ),
	  9 => 
      array (
        'city_name' => '琼中',
      ),
	  10 => 
      array (
        'city_name' => '陵水',
      ),
	  11 => 
      array (
        'city_name' => '东方',
      ),
	  12 => 
      array (
        'city_name' => '定安',
      ),
	   13 => 
      array (
        'city_name' => '澄迈',
      ),
	   14=> 
      array (
        'city_name' => '白沙',
      ),
	   15 => 
      array (
        'city_name' => '儋州',
      ),
	   12 => 
      array (
        'city_name' => '定安',
      ),	 	 	 
    ),
  ),
  22 => 
  array (
    'province_name' => '重庆',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '重庆',
      ),
    ),
  ),
  23 => 
  array (
    'province_name' => '四川',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '成都',
      ),
      2 => 
      array (
        'city_name' => '自贡',
      ),
      3 => 
      array (
        'city_name' => '攀枝花',
      ),
      4 => 
      array (
        'city_name' => '泸州',
      ),
      5 => 
      array (
        'city_name' => '德阳',
      ),
      6 => 
      array (
        'city_name' => '绵阳',
      ),
      7 => 
      array (
        'city_name' => '广元',
      ),
      8 => 
      array (
        'city_name' => '遂宁',
      ),
      9 => 
      array (
        'city_name' => '内江',
      ),
      10 => 
      array (
        'city_name' => '乐山',
      ),
      11 => 
      array (
        'city_name' => '南充',
      ),
      12 => 
      array (
        'city_name' => '眉山',
      ),
      13 => 
      array (
        'city_name' => '宜宾',
      ),
      14 => 
      array (
        'city_name' => '广安',
      ),
      15 => 
      array (
        'city_name' => '达州',
      ),
      16 => 
      array (
        'city_name' => '雅安',
      ),
      17 => 
      array (
        'city_name' => '巴中',
      ),
      18 => 
      array (
        'city_name' => '资阳',
      ),
      19 => 
      array (
        'city_name' => '阿坝',
      ),
      20 => 
      array (
        'city_name' => '甘孜',
      ),
      21 => 
      array (
        'city_name' => '凉山',
      ),
    ),
  ),
  24 => 
  array (
    'province_name' => '贵州',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '贵阳',
      ),
      2 => 
      array (
        'city_name' => '六盘水',
      ),
      3 => 
      array (
        'city_name' => '遵义',
      ),
      4 => 
      array (
        'city_name' => '安顺',
      ),
      5 => 
      array (
        'city_name' => '铜仁',
      ),
      6 => 
      array (
        'city_name' => '黔西南',
      ),
      7 => 
      array (
        'city_name' => '毕节',
      ),
      8 => 
      array (
        'city_name' => '黔东南',
      ),
      9 => 
      array (
        'city_name' => '黔南',
      ),
    ),
  ),
  25 => 
  array (
    'province_name' => '云南',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '昆明',
      ),
      2 => 
      array (
        'city_name' => '曲靖',
      ),
      3 => 
      array (
        'city_name' => '玉溪',
      ),
      4 => 
      array (
        'city_name' => '保山',
      ),
      5 => 
      array (
        'city_name' => '昭通',
      ),
      6 => 
      array (
        'city_name' => '丽江',
      ),
      7 => 
      array (
        'city_name' => '思茅',
      ),
      8 => 
      array (
        'city_name' => '临沧',
      ),
      9 => 
      array (
        'city_name' => '楚雄',
      ),
      10 => 
      array (
        'city_name' => '红河',
      ),
      11 => 
      array (
        'city_name' => '文山',
      ),
      12 => 
      array (
        'city_name' => '西双版纳',
      ),
      13 => 
      array (
        'city_name' => '大理',
      ),
      14 => 
      array (
        'city_name' => '德宏',
      ),
      15 => 
      array (
        'city_name' => '怒江',
      ),
      16 => 
      array (
        'city_name' => '迪庆',
      ),
    ),
  ),
  26 => 
  array (
    'province_name' => '西藏',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '拉萨',
      ),
      2 => 
      array (
        'city_name' => '昌都',
      ),
      3 => 
      array (
        'city_name' => '山南',
      ),
      4 => 
      array (
        'city_name' => '日喀则',
      ),
      5 => 
      array (
        'city_name' => '那曲',
      ),
      6 => 
      array (
        'city_name' => '阿里',
      ),
      7 => 
      array (
        'city_name' => '林芝',
      ),
    ),
  ),
  27 => 
  array (
    'province_name' => '陕西',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '西安',
      ),
      2 => 
      array (
        'city_name' => '铜川',
      ),
      3 => 
      array (
        'city_name' => '宝鸡',
      ),
      4 => 
      array (
        'city_name' => '咸阳',
      ),
      5 => 
      array (
        'city_name' => '渭南',
      ),
      6 => 
      array (
        'city_name' => '延安',
      ),
      7 => 
      array (
        'city_name' => '汉中',
      ),
      8 => 
      array (
        'city_name' => '榆林',
      ),
      9 => 
      array (
        'city_name' => '安康',
      ),
      10 => 
      array (
        'city_name' => '商洛',
      ),
    ),
  ),
  28 => 
  array (
    'province_name' => '甘肃',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '兰州',
      ),
      2 => 
      array (
        'city_name' => '嘉峪关',
      ),
      3 => 
      array (
        'city_name' => '金昌',
      ),
      4 => 
      array (
        'city_name' => '白银',
      ),
      5 => 
      array (
        'city_name' => '天水',
      ),
      6 => 
      array (
        'city_name' => '武威',
      ),
      7 => 
      array (
        'city_name' => '张掖',
      ),
      8 => 
      array (
        'city_name' => '平凉',
      ),
      9 => 
      array (
        'city_name' => '酒泉',
      ),
      10 => 
      array (
        'city_name' => '庆阳',
      ),
      11 => 
      array (
        'city_name' => '定西',
      ),
      12 => 
      array (
        'city_name' => '陇南',
      ),
      13 => 
      array (
        'city_name' => '临夏',
      ),
      14 => 
      array (
        'city_name' => '甘南',
      ),
    ),
  ),
  29 => 
  array (
    'province_name' => '青海',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '西宁',
      ),
      2 => 
      array (
        'city_name' => '海东',
      ),
      3 => 
      array (
        'city_name' => '海北',
      ),
      4 => 
      array (
        'city_name' => '黄南',
      ),
      5 => 
      array (
        'city_name' => '海南',
      ),
      6 => 
      array (
        'city_name' => '果洛',
      ),
      7 => 
      array (
        'city_name' => '玉树',
      ),
      8 => 
      array (
        'city_name' => '海西',
      ),
    ),
  ),
  30 => 
  array (
    'province_name' => '宁夏',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '银川',
      ),
      2 => 
      array (
        'city_name' => '石嘴山',
      ),
      3 => 
      array (
        'city_name' => '吴忠',
      ),
      4 => 
      array (
        'city_name' => '固原',
      ),
      5 => 
      array (
        'city_name' => '中卫',
      ),
    ),
  ),
  31 => 
  array (
    'province_name' => '新疆',
    'city' => 
    array (
      1 => 
      array (
        'city_name' => '乌鲁木齐',
      ),
      2 => 
      array (
        'city_name' => '克拉玛依',
      ),
      3 => 
      array (
        'city_name' => '吐鲁番',
      ),
      4 => 
      array (
        'city_name' => '哈密',
      ),
      5 => 
      array (
        'city_name' => '昌吉',
      ),
      6 => 
      array (
        'city_name' => '博尔塔拉',
      ),
      7 => 
      array (
        'city_name' => '巴音郭楞',
      ),
      8 => 
      array (
        'city_name' => '阿克苏',
      ),
      9 => 
      array (
        'city_name' => '克孜勒苏',
      ),
      10 => 
      array (
        'city_name' => '喀什',
      ),
      11 => 
      array (
        'city_name' => '和田',
      ),
      12 => 
      array (
        'city_name' => '伊犁哈萨克',
      ),
      13 => 
      array (
        'city_name' => '塔城',
      ),
      14 => 
      array (
        'city_name' => '阿勒泰',
      ),
      15 => 
      array (
        'city_name' => '五家渠',
      ),
    ),
  ),

));
